#! /usr/bin/env python3

# Licensed under CC BY-NC-SA by JCSzamosi ()

'''A script that takes a set of fasta files, a set of weights (weights), a read
length (rdl), and a number (n), and randomly pulls n sequences of length rdl
from the fasta files, weighted per file..'''

#################################### Imports ###################################
import argparse as ap
import random as rn
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

################################### Arguments ##################################

parser = ap.ArgumentParser(description = 'Perform in-silico digestion '+\
							'of any number of genomes and produce a read '+\
							'fasta file. There is rounding involved in taking'+\
							' the right number of sequences from each file, '+\
							'so you might not get exactly the right number of'+\
							' sequences. What can I say? I\'m lazy.')

hstr = 'An integer, giving the desired mean read length. Defaults to 150.'
parser.add_argument('-r','--readlen', help = hstr, type = int, default = 150)

hstr = 'An integer, giving the desired read length standard deviation. '+\
		'Defaults to 0, so all reads are the length given by r. Reads will '+\
		'have an enforced minimum length of 10.'
parser.add_argument('-v','--varlen', help = hstr, type = int, default = 0)

hstr = 'A path to a file that contains a list of all the genome file paths '+\
		'you\'re using. The file must have one path per line. All files must '+\
		'be fasta files. '
parser.add_argument('-f','--fofn', help = hstr, type = str, required = True)

hstr = 'An integer giving the total number of reads desired. Defaults to '+\
		'100,000.'
parser.add_argument('-n','--number', help = hstr, type = int, default = 100000)

hstr = 'An optional path to that will be prepended to all the file names in '+\
		'the fofn. '
parser.add_argument('-i','--inputdir', help = hstr,	type = str, \
					required = False, default = '')

hstr = 'The path to and basename of the output file. Will get .fa or _RX.fq '+\
		'appended to it, depending on -p.'
parser.add_argument('-o','--outfile', help = hstr, type = str, required = True)

hstr = 'Use this parameter to provide a file of weights. The weights need to '+\
		'be numbers, comma-separated, on a single line in the file. If no ' +\
		'file is provided, the sequences will be drawn uniformly from all '+\
		'the genome files. There must be the same number of weights in this '+\
		'file as there are genomes in the fofn.'
parser.add_argument('-w','--weights', help = hstr, type = str, default = '')

hstr = 'The line number to use in the file of weights. If this argument is '+\
		'not used, the first line will be taken. Values are >= 1.'
parser.add_argument('-l','--line', help = hstr, type = int, required = False,
					default = 1)

hstr = 'A flag to indicate that the reads should be paired end. Defaults to '+\
		'False,'
parser.add_argument('-p','--pair', help = hstr, action = 'store_true')

hstr = 'The mean insert length. Default value is 300. Ignored if -p is not set.'
parser.add_argument('-m','--meanlen', help = hstr, type = int, required = False,
					default = 300)

hstr = 'The standard deviation of the insert length. Default value is 100. '+\
		'Ignored if -p is not set.'
parser.add_argument('-s','--sd', help = hstr, type = int, required = False, 
					default = 100)

################################### Functions ##################################

def generate_single_reads(files,n,weights,rdl):
	'''A function that takes a list of strings 'files' that holds fasta file
	names, and integer 'n' that gives the number of reads desired, a list of
	floats between 0 and 1 'weights' that must sum to 1 and gives the weights
	for each of the files, and an integer 'rdl' that gives the read length.'''

	for i in range(len(files)):
		# How many counts am I taking from this file
		count = round(weights[i] * n)

		fnm = files[i]
		recs = SeqIO.parse(fnm, 'fasta')

		# Put all the records in this file into one sequence string
		seq = ''
		for rec in recs:
			seq += str(rec.seq)

		# Create a list of start positions
		starts = []
		for i in range(count):
			starts.append(rn.randrange(len(seq)-150))
		
		# Create a counter and base ID string for the sequences from this genome
		j = 0
		idstr = fnm.split('.')[0]
		if '/' in idstr:
			idstr = idstr.split('/')[-1]
		for start in starts:
			j += 1
			sub = seq[start:start+rdl]
			sub = Seq(sub)
			rec = SeqRecord(seq = sub, id = idstr+'_'+str(j))

			yield(rec)

def generate_paired_reads(files, n, weights, rdl, ml, sd):
	'''A function that takes a list of strings 'files' that holds fasta file
	names, an integer 'n' that gives the number of reads desired, a list of
	floats between 0 and 1 'weights' that must sum to 1 and gives the weights
	for each of the files, an integer 'rdl' that gives the read length, an
	integer, 'ml' that gives the mean desired insert length, and an integer 'sd'
	that gives the standard deviation of the insert lengths. Yields pairs of
	sequence record objects drawn randomly from the files, weighted by the
	weights. The reads will be the minimum of rdl or the insert length and will
	have quality scores of 34.'''

	# Iterate through the files
	for i in range(len(files)):
		# How many pairs from this file
		count = round(weights[i]*n)
		
		# Open the files
		fnm = files[i]
		recs = SeqIO.parse(fnm,'fasta')

		# Put all the records in this file into one sequence string
		seq = ''
		for rec in recs:
			seq += str(rec.seq)

		# Create a list of start positions
		starts = []
		for i in range(count):
			starts.append(rn.randrange(len(seq)))

		# Create a counter and base ID string for the sequences from this genome
		j = 0
		idstr = fnm.split('.')[0]
		if '/' in idstr:
			idstr = idstr.split('/')[-1]

		# Get the sequence
		for start in starts:
			j += 1

			# Create the insert
			insl = round(rn.gauss(ml,sd))	# length
			if (len(seq) - start) < insl:
				start = len(seq) - insl
			ins = seq[start:start+insl]
			ins = Seq(ins)

			# Get the reads
			if insl <= rdl:
				s1 = ins
				s2 = ins.reverse_complement()
				qual = [34]*insl
			else:
				s1 = ins[:rdl]
				s2 = ins.reverse_complement()[:rdl]
				qual = [34]*rdl

			# Turn them into sequence record objects
			rec1 = SeqRecord(s1, id = idstr+'_'+str(j), 
							letter_annotations = {'phred_quality':qual})
			rec2 = SeqRecord(s2, id = idstr+'_'+str(j), 
							letter_annotations = {'phred_quality':qual})

			yield(rec1,rec2)



##################################### Main #####################################
if __name__ == '__main__':

	# Parse the arguments and rename the variables
	args = parser.parse_args()
	fofn = args.fofn			# File containing genome file names
	indir = args.inputdir		# Directory where the genome files live
	if indir and (not indir.endswith('/')):
		indir += '/'			# Make sure the directory name ends with a slash
	outfnm = args.outfile		# Output file
	wtfile = args.weights		# File with the list of weights in it
	num = args.number			# Number of sequences
	rdl = args.readlen			# Read length
	line = args.line			# The line in the weights file
	if line < 1:
		msg = "The line number must be greater than or equal to 1."
		raise ValueError(msg)
	pair = args.pair			# Check if the reads should be paired-end
	if pair:
		ml = args.meanlen	# Mean insert length
		sd = args.sd			# standard deviation of insert length

	# Get the list of file names
	hofns = open(fofn)
	files = hofns.readlines()
	hofns.close()
	if indir:
		for i in range(len(files)):
			files[i] = indir+files[i].strip()

	# Get the weights
	if wtfile:
		hwts = open(wtfile)
		for i in range(line):		# iterate to the correct line
			wtstr = hwts.readline().strip()
		wts = wtstr.split(',')
		# check length
		if len(wts) != len(files):
			msg = 'You must provide the same number of weights as genome files'
			raise Exception(msg)

		# Make the weights numbers
		for i in range(len(wts)):
			try:
				wts[i] = float(wts[i].strip())
			except:
				msg = 'The weights file must contain a single line of ' +\
						'comma-separated numbers.'
				raise ValueError(msg)

		# Normalize the weights
		normwts = []
		for i in wts:
			normwts.append(i/sum(wts))
	else:
		normwts = [1/len(files)] * len(files)	# uniform weights
	
	# Generate all the reads
	reads = []
	if pair:
		for rp in generate_paired_reads(files,num,normwts,rdl,ml,sd):
			reads.append(rp)
	else:
		for read in generate_single_reads(files,num,normwts,rdl):
			reads.append(read)

	# Randomize the order of the reads
	rn.shuffle(reads)
	
	if pair:
		outfnm1 = outfnm+'_R1.fq'
		outfnm2 = outfnm+'_R2.fq'

		r1 = [x[0] for x in reads]
		r2 = [x[1] for x in reads]

		SeqIO.write(r1, outfnm1,'fastq')
		SeqIO.write(r2, outfnm2, 'fastq')
		
	else:
		outfnm += '.fa'
		# Write the reads to the output file
		SeqIO.write(reads,outfnm,'fasta')
