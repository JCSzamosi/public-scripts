#! /usr/bin/env python3

'''A script that takes a text file and reprints it adding a character to the end
of every nth line starting at the ath line.'''

import gzip
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--infile',type = str, required = True)
parser.add_argument('-o','--outfile',type = str, required = True)
parser.add_argument('-s','--string',type = str, required = True, help = \
					'The string to add. Will be prepended by a space because '+\
					'I\'m lazy.')
parser.add_argument('-n',help = 'Add the string to the end of every nth line.',\
					type = int, default = 4)
parser.add_argument('-a',help = 'The line to start on. 1-indexed',type = int, \
					default = 1)

# Parse
args = parser.parse_args()
i = args.infile
o = args.outfile
n = args.n
a = args.a
s = args.string

# Open the infile
if i.endswith('gz'):
	handle = gzip.open(i)
else:
	handle = open(i)

# Open the file to write
if o.endswith('gz'):
	out = gzip.open(o,'w')
else:
	out = open(o,'w')

for j in range(a):
	first = handle.readline()
	if j < (a-1):
		out.write(first)

# Now that we're at the first line:
first = str(first)
first = first.strip()
first = first+' '+s+'\n'
out.write(first)

# Iterate through the rest of the lines
count = 1
for line in handle:
	line = str(line)
	if count % n == 0:
		line = line.strip()
		line = line + ' ' +s+'\n'
		out.write(line)
	else:
		out.write(line)
	count += 1

print('done!')
