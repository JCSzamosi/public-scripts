Scripts
=======

This is a repository of scripts I've written that may be of use to others. Most
of my scripts are written in Python or Python3, and are intended to take
command-line arguments. All scripts licensed under Creative Commons BY-NC-SA.
You may use, alter, and redistribute them as long as you attribute the original
to JCSzamosi with a link back to this repository (), your use is not commercial,
and you attach this notice.

In Silico Digestion
===================

[digest.py3](./digest.py3) digests genomes in Python3 using `random` and
`Biopython` as follows:

The number of reads taken from each genome is set by the user.

* For each genome:
	* Concatenate all sequences from in the fasta file into a single string.
	* For n reads, randomly choose n start points in that string (with 
	  replacement)
	* For paired end reads:
		* For a given mean (default 300) and sd (default 100) insert length, 
		  choose from a normal distribution this insert length. Extract the
		  substring ins = str[start:start+inslen] (I don't love these defaults
		  and will probably change them).
		* For read 1, select the substring ins[0:readlen] (take the whole thing 
		  if inslen <= readlen)
		* Read 2 is as read 1, but on the reverse complement of ins.
	* For single reads:
		* select the substring str[start:start+readlen]
* Once reads have been generated for all genomes, shuffle them using
  `random.shuffle()` (preserving pairing if paired) and write them to a fastq
  file, with a consistent quality score of 34 at all bases

